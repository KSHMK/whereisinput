﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace TESTRE
{
    public partial class MainForm : Form
    {
        [DllImport("MarKish.dll")]
        public static extern bool check(char[] Input);

        public MainForm()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            String Input;
            String Key = "Th1S_1s_";
            int i;
            Input = InputTextbox.Text;
            for (i = 0; i < Key.Length; i++)
                if (Key[i] != Input[i])
                    break;
            if (i == Key.Length)
            {
                try
                {
                    if (check(Input.ToCharArray()))
                        MessageBox.Show("Good Job!");
                    else
                        InputTextbox.Text = "Wrong!";
                }
                catch (Exception ex)
                {
                    if (ex.HResult == -2146233052)
                        InputTextbox.Text = "Wrong!!";
                    else
                        MessageBox.Show(ex.Message);
                        InputTextbox.Text = "Call Admin";
                }
            }
            else
                InputTextbox.Text = "Wrong!!";
        }
    }
}
