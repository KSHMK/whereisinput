#include "Header.h"
HANDLE fhMuKey, fhMuCount;
int fCount, fIsOK;
unsigned char fKey[50] = { 0 };
unsigned char fChk[50] = { 0xe2,0x49,0xd9,0x6f,0xa3,0xf5,0x59,0x2f,0xff,0x8f,0x01,0x31,0xfd,0x21,0x42,0x70,0xc4,0x05,0x54,0x3e,0xb3,0x49,0x91,0x88,0x17,0x50,0x17,0xe6,0x87,0x1e,0xd0,0xc4,0x68 };
extern "C" __declspec(dllexport) bool check(char* input)
{
	HMODULE hMod = NULL;
	FARPROC pProc = NULL;
	LPBYTE pTEB = NULL;
	LPBYTE pPEB = NULL;
	int iTmp, i = 1;
	int iStartTime, iNowTime;
	HANDLE hThreads[2];
	int iReturns[2] = { 0 };

	// set OK
	fIsOK = 1;
	fCount = -1;

	// check string length
	if (strlen(input) != 33)
		return false;
	if (IsDebuggerPresent()) {
		__asm { and eax, 0xff }
		return false;
	}
	// copy string to compare at thread
	memcpy(fKey, input, 33);

	// create Mutex for key and Count to DEADLOCK
	fhMuKey = CreateMutex(NULL, false, NULL);
	fhMuCount = CreateMutex(NULL, false, NULL);
	
	// Ldr
	pProc = GetProcAddress(GetModuleHandle(L"ntdll.dll"), "NtCurrentTeb");
	pTEB = (LPBYTE)(*pProc)();               // address of TEB
	pPEB = (LPBYTE)*(LPDWORD)(pTEB + 0x30);     // address of PEB

	DWORD pLdrSig[4] = { 0xEEFEEEFE, 0xEEFEEEFE, 0xEEFEEEFE, 0xEEFEEEFE };
	LPBYTE pLdr = (LPBYTE)*(LPDWORD)(pPEB + 0xC);
	__try
	{
		while (TRUE)
		{
			if (!memcmp(pLdr, pLdrSig, sizeof(pLdrSig)))
			{
				__asm { and eax, 0xff }
				return false;
				break;
			}

			pLdr++;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		;
	}

	// Create Thread
	hThreads[0] = CreateThread(NULL, 0, fTh1, NULL, 0, (LPDWORD)&iTmp);
	hThreads[1] = CreateThread(NULL, 0, fTh2, NULL, 0, (LPDWORD)&iTmp);
	
	// Process Heap - Flags
	LPBYTE pHeap = (LPBYTE)*(LPDWORD)(pPEB + 0x18);
	DWORD dwFlags = *(LPDWORD)(pHeap + 0xC);
	if (dwFlags != 0x2) {
		__asm { and eax, 0xff }
		return false;
	}


	// Set Timer 3 sec
	iStartTime = GetTickCount();
	while (1) {
		// Is Exit?
		GetExitCodeThread(hThreads[0], (LPDWORD)&iReturns[0]);
		GetExitCodeThread(hThreads[1], (LPDWORD)&iReturns[1]);
		iNowTime = GetTickCount();
		if (iReturns[0] == 1 && iReturns[1] == 1)
			break;
		if (iNowTime - iStartTime > 3000)
		{
			// Exit Threads
			TerminateThread(hThreads[0], 0);
			TerminateThread(hThreads[1], 0);
			fIsOK = 0;
			break;
		}
	}
	if (i < 0)
	{
		__asm {
			xor esp, esp
			mov eax, 1
		}
	}

	// NtGlobalFlag
	DWORD dwNtGlobalFlag = *(LPDWORD)(pPEB + 0x68);
	if ((dwNtGlobalFlag & 0x70) == 0x70)
	{
		__asm { and eax, 0xff }
		return false;
	}

	// Look https://bitbucket.org/KSHMK/c-c-bool-confusion
	__asm { and eax, 0xff }

	if (fIsOK == 1)
		return true;
	else
		return false;
}
DWORD WINAPI fTh1(LPVOID lpParam)
{
	int i, iTmp,k;
	mysrand(0);
	for (i = 0; i < 33; i++) {
		if (!fIsOK)
			return 1;
		WaitForSingleObject(fhMuKey, INFINITE);
		iTmp = fKey[i];
		k = (myrand() % 8);
		fKey[i] = ROR(iTmp, k);
		fKey[i] ^= (myrand() & 0xFF);
		WaitForSingleObject(fhMuCount, INFINITE);
		ReleaseMutex(fhMuKey);
		fCount++;
		ReleaseMutex(fhMuCount);
		mysrand(myrand());
	}
	fCount++;
	return 1;
}
DWORD WINAPI fTh2(LPVOID lpParam)
{
	int i;
	for (i = 0; i < 33; i++) {
		if (i > fCount - 1)
		{
			i--;
			continue;
		}
		WaitForSingleObject(fhMuCount, INFINITE);
		WaitForSingleObject(fhMuKey, INFINITE);
		if ((fKey[i + 1] & 0xff) != ((fKey[i] ^ fChk[i]) & 0xff))
		{
			fIsOK = 0;
			return 1;
		}
		ReleaseMutex(fhMuKey);
		ReleaseMutex(fhMuCount);
	}
	return 1;
}