#ifndef __KSHMK__
#define __KSHMK__
#include<Windows.h>
#include<time.h>
#include<stdio.h>
#define ROR(x,y) ((((x) >> (y)) & 0xFF) | (((x) << (8 - (y))) & 0xFF))
#define ROL(x,y) ((((x) << (y)) & 0xFF) | (((x) >> (8 - (y))) & 0xFF))
DWORD WINAPI fTh1(LPVOID lpParam);
DWORD WINAPI fTh2(LPVOID lpParam);
DWORD WINAPI sTh1(LPVOID lpParam);
DWORD WINAPI sTh2(LPVOID lpParam);
void second(void);
bool secondchecker(char*);
static unsigned long next = 1;

/* RAND_MAX assumed to be 32767 */
int myrand(void);

void mysrand(unsigned seed);
#endif